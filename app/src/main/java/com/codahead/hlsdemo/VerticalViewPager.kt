package com.codahead.hlsdemo

import android.content.Context
import android.util.AttributeSet
import android.util.Log
import android.view.View
import androidx.viewpager.widget.ViewPager
import android.view.MotionEvent
import android.widget.Scroller
import android.view.animation.Interpolator
import android.view.animation.DecelerateInterpolator

/**
 * Created by Andrzej Ludkiewicz on 2020-01-20.
 * Copyright (c) 2020 Codahead. All rights reserved.
 */

class VerticalViewPager : ViewPager {

    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attributeSet: AttributeSet) : super(context, attributeSet) {
        init()
    }

    private var mScroller: FixedSpeedScroller? = null

    override fun setCurrentItem(item: Int) = setCurrentItem(item, false)

    fun init() {
        try {
            val viewpager = ViewPager::class.java
            val scroller = viewpager.getDeclaredField("mScroller")
            scroller.isAccessible = true
            mScroller = FixedSpeedScroller(
                context,
                DecelerateInterpolator()
            )
            scroller.set(this, mScroller)
        } catch (ignored: Exception) {

        }

        setPageTransformer(true, VerticalViewPagerTransform())
        overScrollMode = View.OVER_SCROLL_NEVER
    }
    private fun swapXY(event: MotionEvent): MotionEvent {
        val newX = event.y
        val newY = event.x

        event.setLocation(newX, newY)
        return event
    }

    override fun onInterceptTouchEvent(ev: MotionEvent): Boolean {
        val intercept = super.onInterceptTouchEvent(swapXY(ev))
        swapXY(ev)
        return intercept
    }


    override fun onTouchEvent(ev: MotionEvent): Boolean {
        return super.onTouchEvent(swapXY(ev))
    }

    private inner class VerticalViewPagerTransform : PageTransformer {

        override fun transformPage(page: View, position: Float) {

            if (position < -1) {
                page.alpha = 0f
            } else if (position <= 1) {
                page.alpha = 1f
                page.translationX = page.width * -position
                page.translationY = position * page.height

            } else if (position > 1) {
                page.alpha = 0f
            }
        }
    }

    private inner class FixedSpeedScroller : Scroller {

        private var mDuration = 300

        constructor(context: Context, interpolator: Interpolator) : super(context, interpolator) {}

        override fun startScroll(startX: Int, startY: Int, dx: Int, dy: Int, duration: Int) {
            // Ignore received duration, use fixed one instead
            super.startScroll(startX, startY, dx, dy, mDuration)
        }

        override fun startScroll(startX: Int, startY: Int, dx: Int, dy: Int) {
            // Ignore received duration, use fixed one instead
            super.startScroll(startX, startY, dx, dy, mDuration)
        }

        fun setScrollDuration(duration: Int) {
            mDuration = duration
        }
    }

    companion object {
        private val TAG = "TIKTOK"
    }
}
