package com.codahead.hlsdemo.adapter

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.codahead.hlsdemo.fragment.VideoFragment

/**
 * Created by Andrzej Ludkiewicz on 2020-01-20.
 * Copyright (c) 2020 Codahead. All rights reserved.
 */

class VideoFragmentCollectionAdapter(fm: FragmentManager?, private var list: ArrayList<String>, private var cloudFrontURL: String): FragmentStatePagerAdapter(fm){

    override fun getItem(position: Int): Fragment {

        val videoFragment = VideoFragment()
        val bundle = Bundle()
        bundle.putString("hls", list[position])
        bundle.putString("cloudFrontURL", cloudFrontURL)
        videoFragment.arguments = bundle
        return videoFragment
    }

    override fun getCount(): Int {
        return list.size
    }

}