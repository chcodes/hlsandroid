package com.codahead.hlsdemo

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.net.wifi.WifiManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.viewpager.widget.ViewPager
import com.amazonaws.mobile.auth.core.IdentityManager
import com.amazonaws.mobile.client.AWSMobileClient
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.amazonaws.mobile.auth.core.IdentityHandler
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility
import com.amazonaws.regions.Region
import com.amazonaws.services.s3.AmazonS3Client
import com.amazonaws.util.IOUtils
import com.codahead.hlsdemo.adapter.VideoFragmentCollectionAdapter
import com.codahead.hlsdemo.fragment.VideoFragment
import com.github.angads25.toggle.interfaces.OnToggledListener
import com.github.angads25.toggle.model.ToggleableView
import com.github.angads25.toggle.widget.LabeledSwitch
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONTokener
import java.io.File
import java.io.FileInputStream
import java.io.IOException
import java.util.*
import kotlin.collections.ArrayList

@Suppress("DEPRECATED_IDENTITY_EQUALS")
class MainActivity : AppCompatActivity(), IdentityHandler{

    private val LOG_TAG = "TIKTOK"
    private val CONTENT_INDEX_KEY = "content/index.json"
    private val REQUEST_VIDEO = 308       // arbitrary request type id
    private val REQUEST_PERMISSIONS = 403 // arbitrary request type id

    private var listOfVideo = ArrayList<String>()
    private var userIdentity = ""
    private var cloudFrontURL: String = ""

    private lateinit var viewPager: VerticalViewPager
    private lateinit var adapter: VideoFragmentCollectionAdapter
    private var lastPosition: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val uploadButton = findViewById(R.id.uploadButton) as FloatingActionButton

        uploadButton.setOnClickListener{
            recordVideo()
        }
        val labeledSwitch = findViewById(R.id.switcher) as LabeledSwitch
        labeledSwitch.setOnToggledListener(object: OnToggledListener {
            override fun onSwitched(toggleableView: ToggleableView?, isOn: Boolean) {
                when(isOn){
                    true -> {
                        val wifiManager = getSystemService(Context.WIFI_SERVICE) as WifiManager
                        wifiManager.setWifiEnabled(true)
                        showToast("Wi-Fi enabled!")
                    }
                    false -> {
                        val wifiManager = getSystemService(Context.WIFI_SERVICE) as WifiManager
                        wifiManager.setWifiEnabled(false)
                        showToast("Wi-Fi disabled!")
                    }
                }
            }
        });

        AWSMobileClient.getInstance().initialize(this).execute()
        val identityManager = IdentityManager.getDefaultIdentityManager()
        identityManager.getUserID(this)
    }

    override fun onResume() {
        super.onResume()
        Log.d(LOG_TAG, "onResume")
        reloadContentIndex()
    }

    override fun onPause() {
        super.onPause()
        Log.d(LOG_TAG, "onPause")
    }

    fun showToast(text: String) {
        Toast.makeText(
            this, text,
            Toast.LENGTH_SHORT
        ).show()
    }

    private fun initAdapterViewPager(list: ArrayList<String>) {

        viewPager = findViewById(R.id.pager)
        viewPager.offscreenPageLimit = 3
        adapter = VideoFragmentCollectionAdapter(supportFragmentManager, list, cloudFrontURL)
        viewPager.adapter = adapter
        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener{
            override fun onPageScrollStateChanged(state: Int) {
                when(state){
                    1 -> {
                        lastPosition = viewPager.currentItem
                    }

                    0 -> {
                        val last: VideoFragment? = (adapter.instantiateItem(viewPager, lastPosition) as? VideoFragment)
                        last!!.pause()

                        val current: VideoFragment? = (adapter.instantiateItem(viewPager, viewPager.currentItem) as? VideoFragment)
                        current!!.start()

                        if(viewPager.currentItem == lastPosition) viewPager.setCurrentItem(0, false)
                    }
                }
            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int){
            }

            override fun onPageSelected(position: Int){
            }
        })
    }

    private fun reloadContentIndex() {
        try {
            val contentManagerConfig = AWSMobileClient.getInstance()
                .configuration
                .optJsonObject("ContentManager")
            val bucket = contentManagerConfig.getString("Bucket")
            val region = contentManagerConfig.getString("Region")
            cloudFrontURL = contentManagerConfig.getString("CloudFrontURL")

            val outputDir = cacheDir
            val outputFile = File.createTempFile("index", ".json", outputDir)

            val s3 = AmazonS3Client(AWSMobileClient.getInstance().credentialsProvider)
            s3.setRegion(Region.getRegion(region))
            val transferUtility = TransferUtility(s3, applicationContext)

            val observer = transferUtility.download(
                bucket,
                CONTENT_INDEX_KEY,
                outputFile)

            observer.setTransferListener(object : TransferListener {
                override fun onStateChanged(id: Int, state: TransferState) {
                    Log.d(LOG_TAG, "S3 Download State change : $state")

                    if (TransferState.COMPLETED == state) {
                        try {
                            val contentsIndex = IOUtils.toString(FileInputStream(outputFile))
                            val jsonArray = JSONTokener(contentsIndex).nextValue() as JSONArray
                            if (jsonArray.length() <= 0) {
                                this.onError(id, IllegalStateException("No videos available."))
                                return
                            }
                            jsonArray.get(0)
                            listOfVideo = converterJsonArrayToList(jsonArray)
                            initAdapterViewPager(listOfVideo)
                        } catch (e: IOException) {
                            this.onError(id, e)
                        } catch (e: JSONException) {
                            this.onError(id, e)
                        }
                    }
                }

                override fun onProgressChanged(id: Int, bytesCurrent: Long, bytesTotal: Long) {
                    Log.d(LOG_TAG, "S3 Download progress : $bytesCurrent")
                }

                override fun onError(id: Int, ex: Exception) {
                    Log.e(LOG_TAG, "FAILED : " + ex.message, ex)

                    if (ex.message!!.contains("key does not exist")) {
                        Log.d(LOG_TAG, "No content index")
//                        (findViewById(R.id.textView_playing) as TextView).text =
//                            "CONTENT INDEX IS NOT AVAILABLE\n"
                        return
                    }
                }
            })
        } catch (e: JSONException) {
            Log.e(LOG_TAG, e.message, e)
        } catch (e: IOException) {
            Log.e(LOG_TAG, e.message, e)
        }
    }

    /**
     * Called when Amazon Cognito User Identity has been loaded.
     *
     * @param identityId user identity
     */
    override fun onIdentityId(identityId: String) {
        Log.d(LOG_TAG, "Identity : $identityId")
        userIdentity = identityId
    }

    private fun converterJsonArrayToList(jsonArray: JSONArray): ArrayList<String>{
        val list = ArrayList<String>()
        val len = jsonArray.length()
        for (i in 0 until len)
        {
            list.add(jsonArray.get(i).toString())
        }

        return list
    }

    /**
     * Called when an error occurs while trying to load Amazon Cognito User Identity.
     *
     * @param exception exception
     */
    override fun handleError(exception: Exception) {
        Log.e(LOG_TAG, exception.message, exception)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        Log.d(LOG_TAG, "onActivityResult: $resultCode $data")

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_VIDEO) {
                val videoUri = data!!.data
                val projection = arrayOf(MediaStore.Video.VideoColumns.DATA)
                val cursor = contentResolver.query(videoUri!!, projection, null, null, null)

                var vidsCount = 0
                var filename: String? = null

                if (cursor != null) {
                    cursor.moveToFirst()
                    vidsCount = cursor.count

                    do {
                        filename = cursor.getString(0)
                    } while (cursor.moveToNext())

                    val file = File(filename!!)

                    if (file != null &&
                        file.exists() &&
                        file.length() > 0
                    ) {
                        Log.d(LOG_TAG, "Video File : " + file.name)
                        uploadVideoFile(file)
                    } else {
                        Log.d(LOG_TAG, "No video produced.")
                    }
                } else {
                    Log.d(LOG_TAG, "No video produced.")
                }
            }
        }
    }

    private fun recordVideo() {
        if (checkSelfPermission(Manifest.permission.CAMERA) !== PackageManager.PERMISSION_GRANTED || checkSelfPermission(
                Manifest.permission.READ_EXTERNAL_STORAGE
            ) !== PackageManager.PERMISSION_GRANTED
        ){
            requestPermissions(
                arrayOf(Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE),
                REQUEST_PERMISSIONS)
            return
        }
        launchVideoRecord()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_PERMISSIONS) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                launchVideoRecord()
            } else {
                Log.e(LOG_TAG, "No permissions to use camera.")
            }
        }
    }

    private fun launchVideoRecord() {
        val intent = Intent(MediaStore.ACTION_VIDEO_CAPTURE)

        if (intent.resolveActivity(packageManager) != null) {
            startActivityForResult(intent, REQUEST_VIDEO)
        }
    }

    private fun uploadVideoFile(file: File) {
        Log.d(LOG_TAG, "uploadVideoFile: $file")

        val activity = this

        try {
            val contentManagerConfig = AWSMobileClient.getInstance()
                .configuration
                .optJsonObject("ContentManager")
            val bucket = contentManagerConfig.getString("Bucket")
            val region = contentManagerConfig.getString("Region")

            val s3 = AmazonS3Client(AWSMobileClient.getInstance().credentialsProvider)
            s3.setRegion(Region.getRegion(region))
            val transferUtility = TransferUtility(s3, applicationContext)
            val objectKey = "private/" + userIdentity + "/" + Date().time + ".mp4"
            val observer = transferUtility.upload(
                bucket,
                objectKey,
                file
            )

            observer.setTransferListener(object : TransferListener {
                override fun onStateChanged(id: Int, state: TransferState) {
                    Log.d(LOG_TAG, "S3 Upload State change : $state")

                    if (TransferState.COMPLETED == state) {
                        val builder = AlertDialog.Builder(activity)
                        builder.setTitle("Video Upload")
                            .setMessage("Your file has been uploaded.")
                            .setPositiveButton(android.R.string.ok,
                                DialogInterface.OnClickListener { dialog, which ->

                                })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show()
                    }
                }

                override fun onProgressChanged(id: Int, bytesCurrent: Long, bytesTotal: Long) {
                    Log.d(LOG_TAG, "S3 Upload progress : $bytesCurrent")
                }

                override fun onError(id: Int, ex: Exception) {
                    Log.e(LOG_TAG, "FAILED : " + ex.message, ex)

                    val builder = AlertDialog.Builder(activity)
                    builder.setTitle("Video Upload")
                        .setMessage("Your file upload has failed : " + ex.message)
                        .setPositiveButton(android.R.string.ok,
                            DialogInterface.OnClickListener { dialog, which ->

                            })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show()
                }
            })
        } catch (e: JSONException) {
            Log.e(LOG_TAG, e.message, e)
        }
    }


}
