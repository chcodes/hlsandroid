package com.codahead.hlsdemo.fragment

import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.codahead.hlsdemo.R
import com.devbrackets.android.exomedia.core.video.scale.ScaleType
import com.devbrackets.android.exomedia.listener.OnCompletionListener
import com.devbrackets.android.exomedia.listener.OnErrorListener
import com.devbrackets.android.exomedia.listener.OnPreparedListener
import com.devbrackets.android.exomedia.ui.widget.VideoView
import java.lang.Exception
import android.widget.MediaController
import com.devbrackets.android.exomedia.ExoMedia
import com.devbrackets.android.exomedia.ui.widget.VideoControls

class VideoFragment: Fragment(),
    OnPreparedListener, OnErrorListener, OnCompletionListener {

    private val LOG_TAG = "TIKTOK"
    private val CONTENT_FOLDER = "/content/"
    private val CONTENT_FILENAME = "/default.m3u8"

    private var timerHandler: Handler? = null
    private var timerRunnable: Runnable? = null

    private lateinit var uri: String
    private lateinit var cloudFrontURL: String
    private lateinit var videoView: VideoView
    private var prepared: Boolean = false
    private var checkCurrent: Boolean = false
    private var stateMode: Boolean = false

    private lateinit var videoContext: VideoControls

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view =  inflater.inflate(R.layout.fragment_video, container, false)
        videoView = view.findViewById(R.id.videoView) as VideoView

        //videoContext!!.visibility = View.INVISIBLE
        uri = arguments?.getString("hls")!!
        cloudFrontURL = arguments?.getString("cloudFrontURL")!!
        setupVideoView(cloudFrontURL, uri)

        videoContext = videoView.videoControls!!
        videoContext.setTitle("HTTP Live Streaming")
        videoContext.setDescription(uri)
        videoContext.setSubTitle("Codahead demo app")
        videoContext.showLoading(false)
        startTimer()

        videoView.setOnTouchListener(object : View.OnTouchListener{
            override fun onTouch(p0: View?, p1: MotionEvent?): Boolean {

                val action = p1!!.getActionMasked()
                when (action) {

                    (MotionEvent.ACTION_DOWN) -> {
                        if (stateMode) pause() else start()
                        return true
                    }
                    (MotionEvent.ACTION_MOVE) -> {
                        Log.d(LOG_TAG, "Action was MOVE")
                        return true
                    }
                    (MotionEvent.ACTION_UP) -> {
                        Log.d(LOG_TAG, "Action was UP")
                        return true
                    }
                    (MotionEvent.ACTION_CANCEL) -> {
                        Log.d(LOG_TAG, "Action was CANCEL")
                        return true
                    }
                    (MotionEvent.ACTION_OUTSIDE) -> {
                        Log.d(LOG_TAG, ("Movement occurred outside bounds " + "of current screen element"))
                        return true
                    }
                }
                return true
            }
        })
        return view
    }

    private fun setupVideoView(hostingURL: String, contentId: String) {
        // Make sure to use the correct VideoView import

        videoView.reset()
        videoView.setOnPreparedListener(this)
        videoView.setOnErrorListener(this)
        videoView.setScaleType(ScaleType.CENTER_CROP)
        videoView.setOnCompletionListener(this)

        val uri = Uri.parse(hostingURL + CONTENT_FOLDER + contentId + CONTENT_FILENAME)
        Log.d(LOG_TAG, "URI: ${uri}")

        videoView.setVideoURI(uri)
        videoView.requestFocus()
    }

    fun start(){
        if (prepared){
            Log.d(LOG_TAG, "START $uri")
            videoView.start()
        }
        checkCurrent = true
        stateMode = true
    }

    fun pause() {
        Log.d(LOG_TAG, "PAUSE $uri")
        stateMode = false
        videoView.pause()

    }

    override fun onCompletion() {
        Log.d(LOG_TAG, "onCompletion $uri")
        videoView.restart()
    }

    /**
     * Called when video view encounters an error.
     *
     * @param e exception
     * @return true if the error is handled, else false
     */
    override fun onError(e: Exception): Boolean {
        Log.e(LOG_TAG, "Video Load Failed: " + e.message, e)

        //(findViewById(R.id.textView_playing) as TextView).text = "COMING SOON\n$loadingContentId\n"

        return false
    }

    /**
     * Called when video is done loading and is ready to play.
     */
    override fun onPrepared() {
        Log.d(LOG_TAG, "onPrepared ${uri}")
        videoView.visibility = View.VISIBLE
        prepared = true
        if(checkCurrent) videoView.start()
    }

    private fun startTimer() {
        timerHandler = Handler()

        timerRunnable = object : Runnable {
            override fun run() {
                Log.d(LOG_TAG, "timer expired ${videoView.availableTracks}")
                videoContext.show()
                timerHandler!!.postDelayed(this, 1800)
            }
        }

        timerHandler!!.post(timerRunnable)
    }
}
