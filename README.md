# Repo for article: Creation of a pipeline for downloading and transcoding video to HLS format and broadcasting it on Android Devices using Amazon services

Android application that uploads video to Amazon S3 and plays transcoded video (similar to popular video apps like TikTok, Musical.ly, Snapchat).

![Demo](readme-images/demo.gif)

## Requirements

* AWS Account
* [Android Studio 3.5](https://developer.android.com/studio) - for building demo app
* Kotlin 1.3.50

## Steps

* Install Android Studio version 3.5 or higher
* Download configuration file from your AWS Mobile Hub project
* Put awsconfiguration.json file in folder app ▸ src ▸ main ▸ res ▸ raw 
* Build Mobile App

## Author

Andrzej Ludkiewicz - Codahead

## Attribution

This project makes use of the following projects which are available under Apache 2.0 license.
- [ExoMedia - https://github.com/brianwernick/ExoMedia](https://github.com/brianwernick/ExoMedia)
- [Android-toggle - https://github.com/Angads25/android-toggle](https://github.com/Angads25/android-toggle)
